def parse_marks(file_path):
    students = []
    with open(file_path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            parts = line.split()
            for i in range(0, len(parts), 4):
                name = parts[i]
                marks = list(map(int, parts[i+1:i+4]))
                students.append((name, marks))
    return students

def compare_students(marks1, marks2):
    max_m1 = max(marks1)
    min_m1 = min(marks1)
    max_m2 = max(marks2)
    min_m2 = min(marks2)
    
    if max_m1 < min_m2:
        return "lesser"
    elif min_m1 > max_m2:
        return "greater"
    else:
        return "mixed"

def rank_students(students):
    consistent_ranks = []
    mixed_pairs = []
    
    n = len(students)
    for i in range(n):
        for j in range(i+1, n):
            comparison = compare_students(students[i][1], students[j][1])
            if comparison == "greater":
                consistent_ranks.append((students[j][0], students[i][0]))
            elif comparison == "lesser":
                consistent_ranks.append((students[i][0], students[j][0]))
            else:
                mixed_pairs.append((students[i][0], students[j][0]))
    
    return consistent_ranks, mixed_pairs

def build_graph(ranks):
    from collections import defaultdict
    graph = defaultdict(list)
    for a, b in ranks:
        graph[a].append(b)
    return graph

def topological_sort(graph):
    from collections import deque
    
    indegree = {node: 0 for node in graph}
    for node in graph:
        for neighbor in graph[node]:
            if neighbor not in indegree:
                indegree[neighbor] = 0
            indegree[neighbor] += 1
    
    queue = deque([node for node in indegree if indegree[node] == 0])
    sorted_list = []
    
    while queue:
        node = queue.popleft()
        sorted_list.append(node)
        for neighbor in graph[node]:
            indegree[neighbor] -= 1
            if indegree[neighbor] == 0:
                queue.append(neighbor)
    
    return sorted_list

def format_output(consistent_ranks, mixed_pairs):
    consistent_graph = build_graph(consistent_ranks)
    consistent_order = topological_sort(consistent_graph)
    
    consistent_output = " < ".join(consistent_order)
    
    mixed_output = []
    if mixed_pairs:
        mixed_pairs_sorted = sorted(mixed_pairs, key=lambda x: (x[0], x[1]))
        for a, b in mixed_pairs_sorted:
            mixed_output.append(f"{a} < {b}")

    return f"#{consistent_output}\n#" + "\n".join(mixed_output)

def main():
    file_path = 'studentRanking.txt' 
    students = parse_marks(file_path)
    consistent_ranks, mixed_pairs = rank_students(students)
    
    consistent_pairs = []
    consistent_students = set()
    
    for a, b in consistent_ranks:
        consistent_pairs.append((a, b))
        consistent_students.add(a)
        consistent_students.add(b)
    
    mixed_pairs_filtered = [(a, b) for a, b in mixed_pairs if a not in consistent_students and b not in consistent_students]

    output = format_output(consistent_pairs, mixed_pairs_filtered)
    print(output)

if __name__ == "__main__":
    main()