file = "studentRanking.txt"

from functools import cmp_to_key

class Student:
    def __init__(self, name, scores):
        self.name = name
        self.scores = scores

def parse_file(file):
    students = []
    with open(file, 'r') as file:
        for line in file:
            parts = line.strip().split(' ')
            name = parts[0]
            scores = list(map(int, parts[1:]))
            students.append(Student(name, scores))
    return students

def compare_students(student1, student2):
    for score1, score2 in zip(student1.scores, student2.scores):
        if score1 <= score2:
            return 1  
    return -1  

def rank_students(students):
    students.sort(key=cmp_to_key(compare_students))
    return students

def print_rankings(students):
    print("Rankings:")
    for rank, student in enumerate(students, start=1):
        print(f"{rank}. {student.name}: {student.scores}")

students = parse_file(file)
ranked_students = rank_students(students)
print_rankings(ranked_students)

