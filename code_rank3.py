def parse_marks(file_path):
    with open(file_path, 'r') as file:
        return [(line.split()[0], list(map(int, line.split()[1:]))) for line in file.readlines()]

def compare_students(marks1, marks2):
    if max(marks1) < min(marks2):
        return "lesser"
    elif min(marks1) > max(marks2):
        return "greater"
    return "mixed"

def rank_students(students):
    consistent, mixed = [], []
    for i in range(len(students)):
        for j in range(i + 1, len(students)):
            result = compare_students(students[i][1], students[j][1])
            if result == "greater":
                consistent.append((students[j][0], students[i][0]))
            elif result == "lesser":
                consistent.append((students[i][0], students[j][0]))
            else:
                mixed.append((students[i][0], students[j][0]))
    return consistent, mixed
 

def main():
    file_path = 'studentRanking.txt'
    students = parse_marks(file_path)
    consistent, mixed = rank_students(students)
   

    output = rank_students(consistent, mixed)
    print(output)

if __name__ == "__main__":
    main()